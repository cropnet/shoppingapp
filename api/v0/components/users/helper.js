const client = require("../../db/client");
const helper = {};

// helper.createUser = function(body) {
//   let user = {
//     "firstName": body.firstName,
//     "lastName": body.lastName,
//     "email": body.email,
//     "password": body.password
//   };
//   let dbName = "shoppingApp";
//   let collName = "users";
//   let clientPromise = client.getClient();
//   return new Promise(function(resolve, reject) {
//     clientPromise.then(function (mongoClient) {
//       let collObj = mongoClient.db(dbName).collection(collName);
//       let insertPromise = collObj.insertOne(user);
//       insertPromise.then(function(insertResult) {
//         resolve({
//           "message": "Registration successful",
//           "code": 200
//         });
//       }).catch(function(err) {
//         reject({
//           "message": "Error in inserting document.",
//           "code": 500
//         });
//       });
//     }).catch(function(err) {
//       console.log(err);
//       reject({
//         "message": "Error in client creation",
//         "code": 500
//       });
//     });
//   })
// }
//

helper.createUser = function(body) {
  let user = {
    "firstName": body.firstName,
    "lastName": body.lastName,
    "email": body.email,
    "password": body.password
  };
  let dbName = "shoppingApp";
  let collName = "users";
  let clientPromise = client.getClient();
  return new Promise(function(resolve, reject) {
    clientPromise.then(function (mongoClient) {
      let collObj = mongoClient.db(dbName).collection(collName);
      let insertPromise = collObj.insertOne(user);
      insertPromise.then(function(insertResult) {
        resolve({
          "message": "Registration successful",
          "code": 200
        });
      }).catch(function(err) {
        reject({
          "message": "Error in inserting document.",
          "code": 500
        });
      });
    }).catch(function(err) {
      reject({
        "message": "Error in client creation",
        "code": 500
      });
    });
  })
}

helper.validateUser = async function(body) {
  let userCheck = {
    "email": body.email,
  };
  let mongoClient = await client.getClientNew();
  let dbName = "shoppingApp";
  let collName = "users";
  let collObj = await mongoClient.db(dbName).collection(collName);
  let results = await collObj.find(userCheck).toArray();
  if (results.length > 0) {
    return {
      "code": 409,
      "message": "Email Exists Already"
    }
  } else {
    return {
      "code": 200
    }
  }
}

module.exports = helper;
