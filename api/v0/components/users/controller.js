const client = require("../../db/client");
const helper = require("./helper");

/**
Creates a user and saves to db
*/
function registerUser(req, res, next) {
  let resPromise = helper.createUser(req.body);
  resPromise.then(function(succRes) {
    return res.status(200).send(succRes);
  }).catch(function(errRes){
    return res.status(500).send(errRes);
  });
}

/**
Checks if the user is already present
*/
async function validateUser(req, res, next) {
  let response  = await helper.validateUser(req.body);
  if  (response.code == 409) {
    return res.status(409).send(response);
  } else if (response.code == 422) {
    return res.status(422).send(response);
  } else {
    next();
  }
}

// 1. use await inside an async function
// 2. use await only to resolve a promise
// 3. async function returns a promise always

module.exports = {
  "registerUser": registerUser,
  "validateUser": validateUser
}
