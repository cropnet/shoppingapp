const express = require('express');
const router = express.Router();
const controller = require('./controller');
const paramValidator = require('../middlewares/paramsValidator');
const simpleAuth = require('../middlewares/simpleAuth');
router.post('/queryCheck',
  // simpleAuth.checkAuth,
  // function (req, res, next) {
  //   req.validateParamsObj = {
  //     body: [
  //       {
  //         name: 'firstName',
  //         isRequired: true,
  //         type: 'string',
  //         maxLength: 20,
  //         minLength: 4
  //       },
  //       {
  //         name: 'lastName',
  //         isRequired: true,
  //         type: 'string',
  //         maxLength: 7,
  //         minLength: 3
  //       },
  //       {
  //         name: 'email',
  //         isRequired: true,
  //         type: 'string',
  //         pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  //       },
  //       {
  //         name: 'password',
  //         isRequired: true,
  //         type: 'string',
  //         maxLength: 10,
  //         minLength: 6
  //       }
  //     ]
  //   };
  //   next();
  // },
  // paramValidator.validateParams,
  // controller.validateUser,
  controller.registerUser);


module.exports = router;
