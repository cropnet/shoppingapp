
/**
let check = {
  query: [
    {
      name: 'firstName',
      type: 'string',
      isRequired: true
    }
  ]
}
*/

function checkQuery(req, res, next) {
  let query = req.query;
  let checkQuery = req.validateParamsObj.query;
  if (checkQuery.length === 0) {
    next();
  }

  for (let queryParam of checkQuery) {
    if (query[queryParam.name] && typeof(query[queryParam.name]) === 'string') {
      continue
    } else {
      return res.status(422).send({
        "message": "Invalid Parameter",
        "error": "Query param "+ queryParam.name + " is not a string"
      });
    }
  }
  next();
}

function checkBody(req, res, next) {
  let body = req.body;
  let checkBody = req.validateParamsObj.body;
  if (checkBody.length === 0) {
    next();
  }
  for (let bodyParam of checkBody) {
    let paramName = bodyParam.name;
    let value = body[paramName];

    let errorObj = {
      "message": "Invalid Body Parameter",
      "error": null
    }
    // required check
    if (value === undefined) {
      errorObj.error = `${paramName} is required but not provided`;
      return res.status(422).send(errorObj);
    }

    if (typeof(value) !== bodyParam.type) {
      errorObj.error = `Expected ${paramName} to be of type ${bodyParam.type}`;
      return res.status(422).send(errorObj);
    }

    if (bodyParam.maxLength != undefined && value.length > bodyParam.maxLength) {
      errorObj.error = `Expected ${paramName} to be of maximum length  ${bodyParam.maxLength}`;
      return res.status(422).send(errorObj);
    }
    if (bodyParam.minLength != undefined && value.length < bodyParam.minLength) {
      errorObj.error = `Expected ${paramName} to be of min length  ${bodyParam.minLength}`;
      return res.status(422).send(errorObj);
    }

    if (bodyParam.pattern != undefined && !bodyParam.pattern.test(value)) {
      errorObj.error = `Parameter ${paramName} does not match the expected pattern`;
      return res.status(422).send(errorObj);
    }
  }
  next();
}

function validateParams(req, res, next) {
  if (req.validateParamsObj.query && req.validateParamsObj.query.length > 0) {
    checkQuery(req, res, next);
  }

  if (req.validateParamsObj.body && req.validateParamsObj.body.length > 0) {
    checkBody(req, res, next);
  }
  next();
}

module.exports = {
  validateParams
}
