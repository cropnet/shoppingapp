const mongodb = require('mongodb');
const mongoConnectionUrl = "mongodb://127.0.0.1:27017";
const mongoClient = mongodb.MongoClient;
const dbName = "shoppingApp";

// function getClient(cb) {
//   mongoClient.connect(mongoConnectionUrl, function(err, client) {
//     if (err) {
//       console.log(err);
//       cb(null);
//     }
//     cb(client);
//   });
// }


// function clientCallback() {
//   let p = mongoClient.connect(mongoConnectionUrl);
//   return p;
// }
//
// function executeClient() {
//   clientCallback(function (client) {
//     if (client == null) {
//       //
//     } else {
//       //
//     }
//   })
// }



// function getClient() {
//   return new Promise(function(resolve, reject) {
//     mongoClient.connect(mongoConnectionUrl, function(err, client) {
//       if (err) {
//         console.log(err);
//         reject(null);
//       }
//       resolve(client);
//     });
//   });
// }



function getClient() {
  let promise = mongoClient.connect(mongoConnectionUrl);
  return promise;
}

async function getClientNew() {
  try {
    let client = await mongoClient.connect(mongoConnectionUrl);
    return client;
  } catch(err) {
    console.log(err);
  };
}

module.exports = {
  "getClient": getClient,
  "getClientNew": getClientNew
}
